package com.example.api;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    TextInputEditText edt_user, edt_word;
    Button btn_l;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edt_user = findViewById(R.id.edt_user);
        edt_word = findViewById(R.id.edt_pass);
        btn_l = findViewById(R.id.btn_login);

        btn_l.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = edt_user.getText().toString().trim();
                String password = edt_word.getText().toString().trim();

                if (email.isEmpty()){
                    Toast.makeText(MainActivity.this, "UserName / Email Is Required", Toast.LENGTH_SHORT).show();
                    edt_user.setError("UserName / Email Is Required");
                    return;
                } else if (password.isEmpty()){
                    Toast.makeText(MainActivity.this, "Password is Required", Toast.LENGTH_SHORT).show();
                    edt_word.setError("Password is Required");
                    return;
                } else {
                    login();
                }
            }
        });
    }

    public void login(){
        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setEmail(edt_user.getText().toString());
        loginRequest.setPassword(edt_word.getText().toString());

        Call<LoginResponse> loginResponseCall = ApiClient.getUserService().userLogin("rasyid@gmail.com", "00000000");
        loginResponseCall.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if (response.isSuccessful()){
                    Toast.makeText(MainActivity.this, "Login Successful", Toast.LENGTH_SHORT).show();
                } else{
                    Toast.makeText(MainActivity.this, "Login Failed", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                Toast.makeText(MainActivity.this, "Throwable " + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}