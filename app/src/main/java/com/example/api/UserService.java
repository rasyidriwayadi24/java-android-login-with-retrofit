package com.example.api;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface UserService {

    @FormUrlEncoded
    @POST("api/v1/auth/login/")
    Call<LoginResponse> userLogin(@Field("email") String email, @Field("password") String password);
}
